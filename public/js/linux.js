export const linux = {
    titulo: "Linux",
    order: [0,1,2,3,4],
    columnas: {
        0: {
            titulo: "🐧 LINUX",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Distro",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            'Mint' : 'https://linuxmint.com/',
                            'Pearos' : 'https://nicec0re.pearos.xyz/',
                            'Fedora' : 'https://getfedora.org/',
                            'Centos' : 'https://www.centos.org/',
                            'Debian' : 'https://www.debian.org/index.es.html',
                            'Ubuntu' : 'https://ubuntu.com/',
                            'Baslinux' : 'http://distro.ibiblio.org/baslinux/',
                            'Archlinux' : 'https://archlinux.org/',
                            'From Scratch' : 'http://www.escomposlinux.org/lfs-es/lfs-es-6.3/',
                            'Distro Watch' : 'https://distrowatch.com/',
                        },
                    },
                },
                1: {
                    estilos: "bg-danger text-white",
                    titulo: "🐺 Servidores",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Contabo' : 'https://contabo.com/en/vps/?utm_source=google&utm_medium=cpc&utm_campaign=vps&gclid=Cj0KCQjw3a2iBhCFARIsAD4jQB1y-kurrWsRpMyJvRU84M3oKTCLBhBa3bnvDmCwgvHW7cHWKoWSN3kaAqYOEALw_wcB',
                            'Hostinger' : 'https://www.hostinger.com/vps-hosting?utm_medium=affiliate&utm_source=17049&utm_campaign=276&session=102bacb4128de20bc9ae40f141d8e3',
                            'a2hosting' : 'https://www.a2hosting.com/vps-hosting/unmanaged/?aid=codeinwp',
                            'Solucionhost' : 'https://www.solucionhost.cl/vps/',
                        },
                    },
                },
                2: {
                    estilos: "bg-warning text-dark",
                    titulo: "📗 Portales",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Muy Linux' : 'https://www.muylinux.com/',
                            'Linux ORG': 'https://www.linux.org/',
                            'Ayuda Linux' : 'https://ayudalinux.com/',
                            'Cheat Sheets' : 'http://www.cheat-sheets.org/',
                            'Linux Adictos' : 'https://www.linuxadictos.com/',
                        },
                    },
                },

            },
        },
        1: {
            titulo: "🐧 LINUX ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Maquinas Virtuales",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            'Os boxes' : 'https://www.osboxes.org/virtualbox-images/',
                            'Máquinas Virtuales' : 'https://descargarmaquinasvirtuales.com/',
                        },
                    },
                },
                1: {
                    estilos: "bg-danger text-dark",
                    titulo: "🐺 Shell",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Fishshell' : 'https://linuxhint.com/install_fish_shell_linux/',
                            'Oh-my-fish' : 'https://github.com/oh-my-fish/oh-my-fish',
                            'Fish Themes' : 'https://github.com/ohmyzsh/ohmyzsh/wiki/Themes',
                            'Install Fish' : 'https://fishshell.com/',
                        },
                    },
                },
                2: {
                    estilos: "bg-warning text-dark",
                    titulo: "📗 DOCKER",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Tutorial ' : 'https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes-es',
                            'Hub Docker' : 'https://hub.docker.com/search?q=',
                            'Notas Técnicas' : 'https://docs.google.com/document/d/1Fsgg_TMPSquo17PZF6slzVCNx_JM2NAJOuA8gAjmaY8/edit#heading=h.7ea1agcov9a',
                       },
                    },
                },

                3: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Generadores",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            'Chmod':'https://chmod-calculator.com/',
                            'Crontab':'http://corntab.com/',
                            'Htpasswd':'http://www.htaccesstools.com/htpasswd-generator/',
                            'Hash md5':'https://passwordsgenerator.net/md5-hash-generator/',
                            'Decode base64':'https://www.base64decode.org/',
                        },
                    },
                },
                4: {
                    estilos: "bg-danger text-dark",
                    titulo: "🐺 Pentesting",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'HBH V2': 'https://hbh.sh/home',
                            'itsecgames': 'http://www.itsecgames.com/training.htm',
                            'hackthebox': 'https://app.hackthebox.com/home',
                            'cryptii.com': 'https://cryptii.com/pipes/rot13-decoder',
                            'overthewire': 'https://overthewire.org/wargames/',
                            'hackthissite': 'https://www.hackthissite.org/',
                            'www.root-me.org': 'https://www.root-me.org/?lang=es',
                            'openwebinars.net': 'https://openwebinars.net/blog/plataformas-para-practicar-y-aprender-hacking-etico/',
                        },
                    },
                },
            },
        },
        2: {
            titulo: "🐧 LINUX ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Tutoriales",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Comando lsof  ' : 'https://geekflare.com/es/lsof-command-examples/#:~:text=lsof%20es%20una%20poderosa%20utilidad,por%20diferentes%20procesos%20en%20ejecuci%C3%B3n.',
                            'Comando ls beluxe ' : 'https://github.com/Peltoche/lsd',
                            'Manipular imágenes ' : 'https://blog.desdelinux.net/como-manipular-imagenes-desde-el-terminal/',
                            'Más de 400 comando' : 'https://blog.desdelinux.net/mas-de-400-comandos-para-gnulinux-que-deberias-conocer/',
                            'Mejore Trucos Linux' : 'https://www.linuxadictos.com/recopilacion-los-44-mejores-trucos-para-linux.html',
                            'Sacer si se reinicio ' : 'https://www.solvetic.com/tutoriales/article/7480-como-saber-cuando-se-apago-o-reinicio-un-servidor-linux/',
                            'Encuentra lo que sea' : 'https://www.linuxtotal.com.mx/index.php?cont=info_admon_022',
                            'Monitoreo de Procesos ' : 'https://itsoftware.com.co/content/monitorear-proceso-linux-arrancarlo-no-esta-corriendo/',
                            'Correos desde consola' : 'https://maslinux.es/enviar-correo-desde-la-terminal-en-maquinas-con-gnu-linux/',
                            'Campos del comando PS ' : 'https://kb.iu.edu/d/afnv',
                            'Config Red Arch Linux' : 'https://wiki.archlinux.org/index.php/Network_configuration_(Espa%C3%B1ol)#Configurar_la_direcci%C3%B3n_IP',
                            'Comando Básicos PACMAN' : 'https://sobrebits.com/guia-de-comandos-basicos-de-pacman-en-archlinux-y-derivadas/',
                            'Comando Rsync, Ejemplos' : 'https://www.proxadmin.es/blog/rsync-10-ejemplos-practicos-de-comandos-rsync/',
                            'Benchmark Sistema Linux ' : 'https://linuxconfig.org/how-to-benchmark-your-linux-system',
                            'Gestionar VM VirtualBox ' : 'https://www.oracle.com/mx/technical-resources/articles/it-infrastructure/admin-manage-vbox-cli.html',
                            'Rendimientos Escritorio ' : 'https://geekland.eu/mejorar-el-rendimiento-de-un-sistema-operativo-de-escritorio-linux/',
                            'Comprimir, descomprimir' : 'http://ecapy.com/comprimir-y-descomprimir-tgz-tar-gz-y-zip-por-linea-de-comandos-en-linux/index.html',
                            'Proyecto Traducción LFS ' : 'http://www.escomposlinux.org/lfs-es/',
                            'Encuentra Cualquier cosa' : 'https://www.linuxtotal.com.mx/index.php?cont=info_admon_022',
                            'Ruta Carpetas Personales ' : 'https://www.linuxadictos.com/cambia-las-rutas-las-carpetas-personales.html',
                            'Volúmenes lógicos RHEL 7 ' : 'https://www.flecharoja.com/blog/2018-04/volumenes-logicos/',
                            'Guía de Instalación Arch ' : 'https://wiki.archlinux.org/index.php/Installation_guide_(Espa%C3%B1ol)',
                            'Generar Certificado https ' : "https://stackoverflow.com/questions/66604487/how-do-i-generate-fullchain-pem-and-privkey-pem",
                        },
                    },
                },
            },
        },
        3: {
            titulo: "🐧 Servidores ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Servidores VPN",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Contabo': 'https://contabo.com/en/vps/?utm_source=google&utm_medium=cpc&utm_campaign=vps&gclid=Cj0KCQjw3a2iBhCFARIsAD4jQB1y-kurrWsRpMyJvRU84M3oKTCLBhBa3bnvDmCwgvHW7cHWKoWSN3kaAqYOEALw_wcB',
                            'SolucionHost': 'https://www.solucionhost.cl/vps/',
                        },
                    },
                },
                1: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Software",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Jdownloader' : 'https://jdownloader.org/es/home/index'
                        },
                    },
                },
                2: {
                    estilos: "bg-fondo02 text-white",
                    titulo: " 🐧 SandBoxs",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'login' : 'https://id.secondlife.com/openid/login'
                        },
                    },
                },
            },
        },
        4: {
            titulo: "🐧 Revistas ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Revistas ",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Lignux' : 'https://lignux.com/',
                            'Mc Libre' : 'https://www.mclibre.org/consultar/documentacion/revistas-years/revistas-2019.html',
                            'Muy Linux' : 'https://www.muylinux.com/',
                            'Linux org' : 'https://www.linux.org/',
                            'Solo Linux' : 'https://www.sololinux.es/',
                            'Ayuda Linux' : 'https://ayudalinux.com/',
                            'Linux Adictos' : 'https://www.linuxadictos.com/',
                            'Linux Magazine' : 'http://www.linux-magazine.com/Online',
                            'Laboratorio Linux' : 'https://laboratoriolinux.es/',
                        },
                    },
                },
            },
        },

    },
};
