export const entretencion = {
    titulo: "Entretencion",
    order: [1,2,3,4,5],
    columnas: {
        1: {
            titulo: "🦉 Lugares",
            estilos: "bg-secondary text-white",
            temas: {
                0: {
                    estilos: "bg-warning text-dark",
                    titulo: "➊ Lugares",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Tahai' : 'https://www.google.com/maps/@-27.1384932,-109.4262753,3a,79y,233.03h,80.52t/data=!3m6!1e1!3m4!1sAF1QipNanUs_p-uHnwL1FueUlkAoETWYtpJbzvlpZFzV!2e10!7i11000!8i5500?hl=es',
                            'Taitao   ' : 'https://www.google.com/maps/@-33.4700021,-70.7083014,3a,75y,197.93h,79.97t/data=!3m6!1e1!3m4!1se5xOfaGvCJry9syOu_4mAQ!2e0!7i16384!8i8192?hl=es',
                            'Anakean ' : 'https://www.google.com/maps/@-27.0719069,-109.3259227,3a,75y,97.7h,89.59t/data=!3m6!1e1!3m4!1sAF1QipN4naM19JNIXu3Pp3UGNQGx-ssaTNXXMXqIq6sI!2e10!7i11000!8i5500?hl=es',
                            'Poko Poko' : 'https://www.google.com/maps/@-27.1449431,-109.4291873,3a,75y,295.85h,92.22t/data=!3m6!1e1!3m4!1sAF1QipNUrB8cN_TfccNhX47buYYRdPy86h_o7l9X6zHN!2e10!7i11000!8i5500?hl=es',
                            'Rano Raraku' : 'https://www.google.com/maps/@-27.1286425,-109.2880706,3a,75y,10.02h,77.15t/data=!3m7!1e1!3m5!1sAF1QipOTjwL8AstXDAxu77uiygunmXJpVjdOQOf9qkFX!2e10!3e11!7i11000!8i5500?hl=es',
                            'Las Shoforas' : 'https://www.google.com/maps/@-33.4453469,-70.6836288,3a,75y,98.78h,85.51t/data=!3m7!1e1!3m5!1sILbK8SHxBoUm5Wbi2nP9Fw!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DILbK8SHxBoUm5Wbi2nP9Fw%26cb_client%3Dmaps_sv.tactile.gps%26w%3D203%26h%3D100%26yaw%3D127.43416%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192?hl=es',
                            'Colegio Elda' : 'https://www.google.com/maps/@-33.4429664,-70.6705959,3a,50.4y,164.62h,94.32t/data=!3m6!1e1!3m4!1svjdRGzDMLZ4WDskVevs4ZA!2e0!7i16384!8i8192?hl=es',
                            'Pasaje Gloria' : 'https://www.google.com/maps/@-33.4495253,-70.7019733,3a,76.7y,178.32h,89.77t/data=!3m6!1e1!3m4!1sOWoADnpDN-eYUHFEduoHxQ!2e0!7i16384!8i8192?hl=es',
                            'San Pablo DASA' : 'https://www.google.com/maps/@-33.4445817,-70.7449817,3a,75y,355.31h,92.6t/data=!3m6!1e1!3m4!1s13yofdRKerR_T0-rERlrCg!2e0!7i13312!8i6656?hl=es',
                            'Negocio Sandra' : 'https://www.google.com/maps/@-27.1493751,-109.4287268,3a,47.5y,133.71h,93.34t/data=!3m6!1e1!3m4!1sAF1QipPihWysHZJVQHD8yPVOqQZD-ZQ79Z6_ega17K5S!2e10!7i11000!8i5500?hl=es',
                            'Salón Curavavi' : 'https://www.google.com/maps/place/Manuel+Larra%C3%ADn+435,+Curacavi,+Curacav%C3%AD,+Regi%C3%B3n+Metropolitana/@-33.4020245,-71.1311383,3a,75y,7.27h,86.84t/data=!3m6!1e1!3m4!1snheX5oskmT_sMbtx61uY8A!2e0!7i13312!8i6656!4m15!1m8!3m7!1s0x9662f4ceb6802ef5:0x480a87f9fa4f5d74!2sManuel+Larra%C3%ADn+435,+Curacavi,+Curacav%C3%AD,+Regi%C3%B3n+Metropolitana!3b1!8m2!3d-33.3988137!4d-71.1274039!16s%2Fg%2F11k59pq_9n!3m5!1s0x9662f4ceb6802ef5:0x480a87f9fa4f5d74!8m2!3d-33.3988137!4d-71.1274039!16s%2Fg%2F11k59pq_9n?hl=es',
                            'Negocio Claudia' : 'https://www.google.com/maps/@-27.1481569,-109.427098,3a,59.7y,187.02h,96.4t/data=!3m6!1e1!3m4!1sAF1QipOFIn2ysUsWU8qUkdpuX85_ThlbhT0Mq9N2e64I!2e10!7i11000!8i5500?hl=es',
                            'Dagoberto Godoy' : 'https://www.google.com/maps/@-33.4391437,-70.7473534,3a,90y,160.11h,91.73t/data=!3m6!1e1!3m4!1sTIJBirn9-ohsOAp-j1w9BA!2e0!7i16384!8i8192?hl=es',
                            'Liceo A71-Maipú' : 'https://www.google.com/maps/@-33.4634852,-70.7013488,3a,75y,121.62h,90.63t/data=!3m6!1e1!3m4!1soTp9b-I6wg3LelSaqIag2w!2e0!7i16384!8i8192?hl=es',
                            'Cabaña - Papudo' : 'https://www.google.com/maps/@-32.5061732,-71.4442282,3a,75y,162.6h,81.28t/data=!3m6!1e1!3m4!1scFUc4tNjly6yJx0ZIRdRig!2e0!7i13312!8i6656?entry=ttu',                            
                            'Tongoy - Cabañas' : 'https://www.google.com/maps/@-30.261737,-71.4879382,3a,42.1y,98.2h,87.66t/data=!3m6!1e1!3m4!1sUcwiaxu7YWofaFxWQpKxYQ!2e0!7i13312!8i6656?entry=ttu',                            
                            'Liceo Amunategui' : 'https://www.google.com/maps/@-33.4435411,-70.6757202,3a,75y,164.73h,93.1t/data=!3m6!1e1!3m4!1s-0FJot-eenh5y8mhqRf8aQ!2e0!7i13312!8i6656?hl=es',
                            'Manuel Larrain 435' : 'https://www.google.com/maps/place/Manuel+Larra%C3%ADn+435,+Curacavi,+Curacav%C3%AD,+Regi%C3%B3n+Metropolitana/@-33.3988216,-71.1272869,3a,90y,277.74h,92.14t/data=!3m6!1e1!3m4!1slo066zINB9ICdFvtfWaBug!2e0!7i13312!8i6656!4m15!1m8!3m7!1s0x9662f4ceb6802ef5:0x480a87f9fa4f5d74!2sManuel+Larra%C3%ADn+435,+Curacavi,+Curacav%C3%AD,+Regi%C3%B3n+Metropolitana!3b1!8m2!3d-33.3988137!4d-71.1274039!16s%2Fg%2F11k59pq_9n!3m5!1s0x9662f4ceb6802ef5:0x480a87f9fa4f5d74!8m2!3d-33.3988137!4d-71.1274039!16s%2Fg%2F11k59pq_9n?hl=es',
                            'Saucache Arica UTA' : 'https://www.google.com/maps/@-18.4876378,-70.294433,3a,39.3y,190.99h,94.89t/data=!3m6!1e1!3m4!1s0oO87YvlQnY9PEwbr3-V5w!2e0!7i13312!8i6656?entry=ttu',
                            'Santa Petronila 581' : 'https://www.google.com/maps/@-33.4464384,-70.7054215,3a,52.4y,112.82h,88.91t/data=!3m6!1e1!3m4!1sziuU61tEE-6y902raLV5qw!2e0!7i16384!8i8192?hl=es',
                            'Nueva Imperial 5115' : 'https://www.google.com/maps/@-33.445949,-70.7056024,3a,49.6y,339.38h,95.5t/data=!3m6!1e1!3m4!1smtH0Qy2bElptLiYM2vRboA!2e0!7i16384!8i8192?hl=es',
                            'Salón Los Arrayames' : 'https://www.google.com/maps/@-33.4291283,-70.742904,3a,81.9y,262.87h,79.46t/data=!3m6!1e1!3m4!1sHybfXM1uWcch9H4EccT9hA!2e0!7i16384!8i8192?hl=es',
                            'Salón Nueva Imperial' : 'https://www.google.com/maps/@-33.4406558,-70.7072322,3a,40.7y,297.71h,86.7t/data=!3m6!1e1!3m4!1sO2P1KYYCAd4TmgER0SR7pQ!2e0!7i16384!8i8192?hl=es',
                            'Universidad Tarapaca' : 'https://www.google.com/maps/@-18.4712069,-70.3114851,3a,79.8y,280.59h,92.42t/data=!3m6!1e1!3m4!1sWbnDRxkPXkNN3SBLKE6mmw!2e0!7i13312!8i6656?entry=ttu',                            
                            'Playa Lisera - Arica' : 'https://www.google.com/maps/@-18.492569,-70.3257048,3a,68.5y,225.83h,84.67t/data=!3m9!1e1!3m7!1sZkpI28lMPeoECEWd3mIGcg!2e0!7i13312!8i6656!9m2!1b1!2i17?entry=ttu',                            
                            'Entrada Primer Salón' : 'https://www.google.com/maps/@-27.1522263,-109.4315794,3a,75y,318.45h,85.91t/data=!3m6!1e1!3m4!1sAF1QipNmw0IKh-t9PC8p715GghUPAe-vhuImrSmwuOPY!2e10!7i11000!8i5500?hl=es',
                            'Colegio Padre Hurtado' : 'https://www.google.com/maps/@-33.4470872,-70.6974832,3a,75y,276.9h,86.53t/data=!3m6!1e1!3m4!1sSNWu0yOoKPUPUE_DiGS10Q!2e0!7i16384!8i8192?hl=es',
                            'Valdivia - Casa Isabel' : 'https://www.google.com/maps/@-39.8359415,-73.2155611,3a,40.4y,289.09h,90.64t/data=!3m6!1e1!3m4!1s2eonJ0AItIP8Af3xrIyVAA!2e0!7i13312!8i6656?hl=es',
                           },
                    },
                },
            },
        },
        2: {
            titulo: "🦉 Musica",
            estilos: "bg-info text-dark",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "Música Youtube",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            "Sade": "https://www.youtube.com/watch?v=nfbNTcL9xw0",
                            "Origen": "https://www.youtube.com/watch?v=vnkiVa4A-F8",
                            "Clásica": "https://www.youtube.com/watch?v=Srlp8fAXz8c",
                            "Blade Runner": "https://www.youtube.com/watch?v=o4pYju_hEOk",
                            "Corazón Valiente": "https://www.youtube.com/watch?v=d5L796w54hk",
                        },
                    },
                },
                1: {
                    estilos: "bg-fondo02 text-white",
                    titulo: " ➌ Radios Online",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Emisoras' : 'https://emisora.cl/',
                            "Try.Brain":"https://try.brain.fm/",
                            'Radio Garden' :  'https://radio.garden/visit/santiago/n4X0myqO',
                            'Radios Online' : 'https://www.radiosonline.cl/',
                            'My Radio Online' : 'https://myradioonline.cl/',
                            'Shoutcast Stream' :  'http://stream11.shoutcastsolutions.com:8239/index.html?sid=1',
                        },
                    },
                },                
                2: {
                    estilos: "bg-fondo03 text-black",
                    titulo: "Origami",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                           'Toro' :   'https://www.youtube.com/watch?v=zI6deL6LA8U',
                           'Login' : 'https://id.secondlife.com/openid/login',
                           'HotKeys' :'https://wiki.secondlife.com/wiki/All_keyboard_shortcut_keys',
                           'MarketPlace' : 'https://marketplace.secondlife.com/?lang=en-US',
                           'Welcome Areas' : 'https://wiki.secondlife.com/wiki/Inworld_Locations_for_Volunteers/es',
                        },
                    },
                }
            },
        },

        3: {
            titulo: "🦉 Peliculas",
            estilos: "bg-info text-dark",
            temas: {
                0: {
                    estilos: "bg-secondary text-white",
                    titulo: "🍁 Peliculas Completas",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            "Bahubali 1": "https://www.youtube.com/watch?v=XOEktshUZC4",
                            "Chappie Robot": "https://www.youtube.com/watch?v=zF0l8VzCMhs",
                            "José de Egipto": "https://www.youtube.com/watch?v=J0ckMoRXIAo",
                            "Jesús de Nazaret": "https://www.youtube.com/watch?v=aNeWzrpk7P0",
                            "Jesús de Nazaret": "https://www.youtube.com/watch?v=aNeWzrpk7P0",
                            "Planeta de los Simios":"https://www.youtube.com/watch?v=ISN_Siz1UO4&t=282s",
                        },
                    },
                },                

               1: {
                    estilos: "bg-warning text-dark",
                    titulo: "🍁 Canales Locales",
                    enlaces: {
                        estilos: "bg-secondary text-warning",
                        sitios: {
                            "TVN": "https://www.youtube.com/watch?v=EF9T7i5my6A",
                            "Canal 13": "https://www.youtube.com/watch?v=yq-3GquQ94E",
                            "Chile Vision": "https://www.youtube.com/watch?v=4lGVulXkxO0",
                            "Meganoticias": "https://www.youtube.com/watch?v=XK-kQW_Y7pY",
                        },
                    },
                },

                2: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 Plataformas Video",
                    enlaces: {
                        estilos: "bg-info text-dark",
                        sitios: {
                            "Viajes":"https://driveandlisten.herokuapp.com/",
                            "Netflix": "https://www.netflix.com/browse",
                            "Youtube": "http://www.youtube.com/",
                            "Cuevana3": "https://www3.cuevana3.ch/",
                            "Media Center EMBY" : "http://192.168.100.14:8096/web/index.html#!/home",
                        },
                    },
                },
                4: {
                    estilos: "bg-warning text-dark",
                    titulo: "➋ Video",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Index 1': 'http://pilarika.no-ip.org/Documentales/',
                            'Index 2': 'http://fedora.linuxman.biz/dataup/MOVIES/hristian/',
                            'Index 3': 'http://bilbohiria.com/docs2/filmak/',
                            'Mundo Guerra': 'https://www.documaniatv.com/documentales/el-mundo-en-guerra/',
                        },
                    },
                },

        
            },
        },

        4: {
            titulo: "🦉 Documentales",
            estilos: "bg-danger text-white",
            temas: {
                0: {
                    estilos: "bg-primary text-white",
                    titulo: "➋ Documentales YOUTUBE",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {

                            "Ven y Mira":"https://www.youtube.com/watch?v=_BWVqR4tX84",
                            "Liberación Campos":"https://www.youtube.com/watch?v=48-qYKjbx08",
                            "Santos y Soldados":"https://www.youtube.com/watch?v=IYHq5i1esxU",
                            "Resistencia Varsovia":"https://www.youtube.com/watch?v=mZz2yUUKZHk",
                            "Bosque de la masacre":"https://www.youtube.com/watch?v=_YhSlkBiP98",
                            "El Guardian de Auschwitz":"https://www.youtube.com/watch?v=VvHdig2bMXw&t=3302s",
                        },
                    },
                },                
                2: {
                    estilos: "bg-warning text-dark",
                    titulo: "➋ Servidores Torrent",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            '1337x.to': 'https://1337x.to/home/',
                            'idope.se': 'https://idope.se/torrent-list/hobbit/',
                            'Pirate-bays.net': 'https://pirate-bays.net/top',
                            'Mejortorrent.rip': 'ttps://www10.mejortorrent.rip/pelicula/21775/toy-story-2',
                        },
                    },
                },                
            },
        },

        5: {
            titulo: "🦉 Album Fotos",
            estilos: "bg-info text-dark",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 Álbunes",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            "Familia":"html/familia.html",
                            "Matrimonio":"html/matrimonio.html",
                        },
                    },
                },
            },
        },
    },
};


























