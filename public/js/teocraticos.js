export const teocraticos = {
    titulo: "Teocraticos",
    order: [1,2,3,4],
    columnas: {
        1: {
            titulo: "🦉 BiBlia Libros",
            estilos: "bg-secondary text-white",
            temas: {
                0: {
                    estilos: "bg-warning text-dark",
                    titulo: "➊ Biblia",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Logos' : 'https://www.logosklogos.com/interlinear/AT/Gn',
                            'Biblia' : 'https://www.jw.org/es/biblioteca/biblia/biblia-estudio/libros/',
                            'wol.jw.org ' : 'https://wol.jw.org/es/wol/binav/r4/lp-s',
                            'Biblia Todo ' : 'https://www.bibliatodo.com/la-biblia',
                            'Biblia Paralela ' : 'https://bibliaparalela.com/1_samuel/6-4.htm',
                            'Textos Explicados' : 'https://www.jw.org/es/ense%C3%B1anzas-b%C3%ADblicas/textos-biblicos/',
                            'Puntos Sobresalientes' : 'https://docs.google.com/document/d/1jdYnXtyTzdhRQdKDelMD4fS8JYrW7tHuErENlM60AmQ/edit',
                        },
                    },
                },
                1: {
                    estilos: "bg-warning text-dark",
                    titulo: "➊ Libros",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                                'Disfrute' : 'https://wol.jw.org/es/wol/publication/r4/lp-s/lff',
                                'Continúe' : 'https://wol.jw.org/es/wol/publication/r4/lp-s/lvs',
                                'Organizados' : 'https://wol.jw.org/es/wol/publication/r4/lp-s/od',
                                'Guía Videos' : 'https://www.jw.org/es/biblioteca/libros/Gu%C3%ADa-de-videos-para-los-cursos-b%C3%ADblicos/Contin%C3%BAe-en-el-amor-de-Dios/',
                                'Publicaciones' : 'https://wol.jw.org/es/wol/library/r4/lp-s/biblioteca/libros',
                                'Adoración Pura' : 'https://wol.jw.org/es/wol/publication/r4/lp-s/rr',
                        },
                    },
                },
            },
        },
        2: {
            titulo: "🦉 Teocráticos y Documentos",
            estilos: "bg-secondary text-white",
            temas: {
                0: {
                    titulo: "Literatura",
                    estilos: "bg-warning text-dark",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'JW.ORG': "https://www.jw.org/es/",
                            'Biblia': "https://wol.jw.org/es/wol/binav/r4/lp-s",
                            'Biblioteca': "https://wol.jw.org/es/wol/h/r4/lp-s",
                            'Notas Estudio':   "https://docs.google.com/document/d/1cmkeauDSM4qLZKpsZXrWSt029dSyvU_fDzocbMI5Xyo/edit",
                            'Libro Disfrute':   "https://wol.jw.org/es/wol/publication/r4/lp-s/lff",
                            'Comandos Alexa':   "https://www.jw.org/es/ayuda-en-l%C3%ADnea/c%C3%B3mo-usar-jw-org/skills-para-amazon-alexa/",
                            'Otros Documentos':"https://drive.google.com/drive/folders/1c1HQiNhijU-KmHBwJ5DqOIoFpvx6xU5A",
                        },
                    },
                },
                1: {
                    titulo: "Películas",
                    estilos: "bg-danger text-white",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Jonás ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-jcm_x_VIDEO",
                            'Josías I ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-jlj_1_VIDEO",
                            'Josías II ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-jlj_2_VIDEO",
                            'Daniel I ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-dlf_1_VIDEO",
                            'Daniel II ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-dlf_2_VIDEO",
                            'Nehemías I ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-jjs_1_VIDEO",
                            'Nehemías II ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-jjs_2_VIDEO",
                            'Camino de la Paz I ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-wpc_1_VIDEO",
                            'Camino de la Paz II ': "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-wpc_2_VIDEO",
                            'Oh, Jehová, en ti confío' : "https://www.jw.org/es/biblioteca/videos/#es/mediaitems/VODMoviesBibleTimes/pub-tiy_x_VIDEO",
                        },
                    },
                },


            },
        },        
        3: {
            titulo: "🦉 Documentos",
            estilos: "bg-info text-dark",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "Articulos Estudio",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Samuel' : 'https://docs.google.com/document/d/1391NTLCRJImC6A8oJhSYhCPQ5extMMgpjKwe-XJz-CY/edit',
                            'Política' : 'https://www.jw.org/es/testigos-de-jehov%C3%A1/preguntas-frecuentes/neutralidad-pol%C3%ADtica/',
                            'Ministerio' : 'https://docs.google.com/spreadsheets/d/1lPTUspe9S_JaKx0aS068J3yo2xW8lNnaRJPEIRuDU9g/edit#gid=0',
                            'Lectura Bíblica' : 'https://separator.mayastudios.com/index.php?t=horz',
                            '¿Cómo es Jehová ?' : 'https://docs.google.com/document/d/1-adxvMu6u8DHPhVCUSNz27pF_eAkWiiOQsRwlIblMuk/edit',
                            'Texto Predicación' : 'https://docs.google.com/document/d/1_nImaOZbxcEOoCt3eWe20SrBVEMb5ES2bnddwHlJDpQ/edit                            ',
                            'Mochila Emergencia' : 'https://wol.jw.org/es/wol/d/r4/lp-s/102017165#h=37-54:0',
                            'Nuevos Publicadores' : 'https://wol.jw.org/es/wol/d/r4/lp-s/1102014938?q=%22nuevos+publicadores%22&p=par#h=9',
                            '¿Qué espera Jehová de mi?' : 'https://docs.google.com/document/d/1cmkeauDSM4qLZKpsZXrWSt029dSyvU_fDzocbMI5Xyo/edit',
                            'Temas Generales de Interés' : 'https://docs.google.com/document/d/1zpIhCTNE8BIhJsr7TnN6g5R_7R-xZ5f8U5wgq66Y_i0/edit',
                        },
                    },
                }
            },
        },
        4: {
            titulo: "🦉 Peliculas",
            estilos: "bg-info text-dark",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "➊ Plataformas ",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Yotube' : 'https://www.youtube.com/',
                            'Netflix' : 'https://www.netflix.com/browse',
                            'Cuevana' : 'https://cuevana3.mov/',
                            'Pluto TV' : 'https://pluto.tv/es/live-tv/gran-hermano-chile?utm_source=Google&utm_medium=PaidSearch&utm_campaign=1002025&utm_content=10008297&gclid=CjwKCAjwv8qkBhAnEiwAkY-ahhCmsiY5r7TZ2LmfKZUseJtrlvCAJymwp3gK_xKUly7tAzrT8g-_ZBoCzpwQAvD_BwE',
                            'Prime Video' : 'https://www.primevideo.com/',
                        },
                    },
                },

                1: {
                    estilos: "bg-warning text-dark",
                    titulo: "➋ Video",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Index 1': 'http://pilarika.no-ip.org/Documentales/',
                            'Index 2': 'http://fedora.linuxman.biz/dataup/MOVIES/hristian/',
                            'Index 3': 'http://bilbohiria.com/docs2/filmak/',
                            'Mundo Guerra': 'https://www.documaniatv.com/documentales/el-mundo-en-guerra/',
                        },
                    },
                },

                2: {
                    estilos: "bg-fondo02 text-white",
                    titulo: " ➌ Radios Online",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Emisoras' : 'https://emisora.cl/',
                            'Radios Online' : 'https://www.radiosonline.cl/',
                            'My Radio Online' : 'https://myradioonline.cl/',
                        },
                    },
                },
            },
        },
    },
};
