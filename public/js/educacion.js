export const educacion = {
    titulo: "Educación",
    order: [2],
    columnas: {
        2: {
            titulo: "🦉 APIS",
            estilos: "bg-info text-dark",
            temas: {
                1: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "④ Laminas Framework ",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'routing' : 'https://docs.laminas.dev/laminas-mvc-console/routing/',
                            'captcha' : 'https://docs.laminas.dev/laminas-form/element/captcha/',
                            'skeleton' : 'https://docs.mezzio.dev/mezzio/v2/getting-started/skeleton/',
                            'Sessiones' : 'https://olegkrivtsov.github.io/using-zend-framework-3-book/html/es/Trabajar_con_sesiones/El_manejador_de_sesiones.html',
                            'quick-start' : 'https://docs.laminas.dev/laminas-mvc/quick-start/',
                            'The MvcEvent' : 'https://docs.laminas.dev/laminas-mvc/mvc-event/',
                            'development-mode' : 'https://stackoverflow.com/questions/45139629/zf3-development-mode-vs-production-mode                            ',
                            'laminas-mvc-skeleton' : 'https://github.com/laminas/laminas-mvc-skeleton',
                            'Curso youtube laminas' : 'https://www.youtube.com/watch?v=xl81xRbTplQ',
                            'laminas Class Request' : 'https://olegkrivtsov.github.io/laminas-api-reference/html/classes/Laminas/Http/PhpEnvironment/Request.html#getBaseUrl()',
                            'Laminas Framework API' : 'https://olegkrivtsov.github.io/laminas-api-reference/html/index.html',
                        },
                    },
                },
            },
        },        
    },
};
