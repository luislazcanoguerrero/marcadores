export const meteorologia = {
    titulo: "Meteorología",
    order: [0,1,2,3,4],
    columnas: {
        0: {
            titulo: "⛈️ Meteorología",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: "Proyectos WMO",
                    enlaces: {
                        estilos: "bg-dark text-white",
                        sitios: {
                            "AFD": "https://download.dwd.de/pub/afd/doc/",
                            "GBON":  "https://wmo.maps.arcgis.com/apps/webappviewer/index.html?id=795bbc05ca8a4da7a5f5f0aebb210aa8&locale=en",
                            "Wis2": "https://docs.wis2box.wis.wmo.int/en/latest/",
                            "NASA": "https://power.larc.nasa.gov/data-access-viewer/",
                            "OSCAR": "https://oscar.wmo.int/surface/",
                            "WDQMS": "https://wdqms.wmo.int/nwp/synop",
                            "Geoos": "https://geoos.org/",
                            "Wis2Box": "https://github.com/wmo-im/wis2box",
                            "CASTEHR": "http://10.12.70.21/index",
                            "Library":  "https://library.wmo.int/index.php?lvl=etagere_see&id=47#.XEoDs1UzaXA",
                            "SMN-Brasil": "http://gisc.inmet.gov.br/dw/",
                            "Curso WIGOS": "https://etrp.wmo.int/course/view.php?id=146",
                            "Notas de Wis": "https://docs.google.com/document/d/1z4BEI2UJDVXFEzYgulPwpWOYSPmOd_hRh4yYCj05A7s/edit#",
                            "SMN-Argentina": "http://dcpc.smn.gov.ar/dw/index.xhtml",
                            "Documentos WMO-WIGOS": "https://login.microsoftonline.com/eaa6be54-4687-40c4-9827-c044bd8e8d3c/oauth2/authorize?client%5Fid=00000003%2D0000%2D0ff1%2Dce00%2D000000000000&response%5Fmode=form%5Fpost&response%5Ftype=code%20id%5Ftoken&resource=00000003%2D0000%2D0ff1%2Dce00%2D000000000000&scope=openid&nonce=D1D380D67FDE77FF75CECFA4F9B6BE957E9F3958429A7CD0%2DF45AF8F753D5CF0DD6FF5D712DBD03BD00F37313AADFA8D3030931057AF374B1&redirect%5Furi=https%3A%2F%2Fwmoomm%2Esharepoint%2Ecom%2F%5Fforms%2Fdefault%2Easpx&state=OD0w&claims=%7B%22id%5Ftoken%22%3A%7B%22xms%5Fcc%22%3A%7B%22values%22%3A%5B%22CP1%22%5D%7D%7D%7D&wsucxt=1&cobrandid=11bd8083%2D87e0%2D41b5%2Dbb78%2D0bc43c8a8e8a&client%2Drequest%2Did=cd84c1a0%2Dc09d%2D6000%2Db67f%2D2389e23d493b",
                        },
                    },
                },
                1: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 WebServices",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            "Ceazamet": "http://www.ceazamet.cl/ws/pop_ws_doc.php",
                            "Pronostico":          "http://archivos.meteochile.gob.cl/portaldmc/meteochile/js/pronostico.json",
                            "RecienteUvb":          "https://climatologia.meteochile.gob.cl/application/productos/recienteUvb",
                            "EstadoRedEma":          "https://climatologia.meteochile.gob.cl/application/productos/estadoRedEma",
                            "EmaResumenDiario":          "https://climatologia.meteochile.gob.cl/application/productos/emaResumenDiario",
                            "EstacionesRedEma":    "https://climatologia.meteochile.gob.cl/application/productos/estacionesRedEma",
                            "MenuTematicoJson":    "https://climatologia.meteochile.gob.cl/application/index/menuTematicoJson",
                            "DatosRecientesEma":    "https://climatologia.meteochile.gob.cl/application/productos/datosRecientesRedEma",
                            "AvisoAlertaAlarma":    "http://archivos.meteochile.gob.cl/portaldmc/meteochile/js/aviso_alerta_alarma.json",
                            "GruposDeEstaciones":    "https://climatologia.meteochile.gob.cl/application/productos/gruposDeEstaciones",
                            "RedEMAClimatologica":    "https://climatologia.meteochile.gob.cl/application/productos/estacionesEnGrupo/RedEMAClimatologica",
                            "EmaResumenDiarioGeo":    "https://climatologia.meteochile.gob.cl/application/productos/emaResumenDiarioGeo",
                            "DatosRecientesRedEma":    "https://climatologia.meteochile.gob.cl/application/productos/datosRecientesRedEma",
                        },
                    },
                },
            },
         },
        1: {
            titulo: "🐧 Servicios Meteorológicos",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 Software",
                    enlaces: {
                        estilos: "bg-info text-dark",
                        sitios: {
                            'AFD' : 'https://download.dwd.de/pub/afd/doc/',
                            'Geoos' : 'https://geoos.org/',
                            'Openwis' : 'http://gisc.kma.go.kr/openwis-user-portal/srv/en/about.home',
                            'GeoNode' : 'http://geonode.meteochile.gob.cl/geoserver/web/;jsessionid=878D6FCABC03776EBDE21216D729CD57',
                        },
                    },
                },       
               
                1: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: "Nacionales",
                    enlaces: {
                        estilos: "bg-fondo02 text-white",
                        sitios: {
                            'DMC': 'https://www.meteochile.gob.cl/PortalDMC-web/index.xhtml',
                            'Inach': 'http://www.redsensoresinach.cl/index.php?pag=mod_estacion&e_cod=ANTCM&p_cod=rlema',
                        },
                    },
                },       


                2: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 PAISES",
                    enlaces: {
                        estilos: "bg-fondo01 text-dark",
                        sitios: {
                            'Mexico ' : 'https://smn.conagua.gob.mx/es/',
                            'Brasil' : 'https://portal.inmet.gov.br/',
                            'España ' : 'http://www.aemet.es/es/portada',
                            'Bolivia' : 'http://senamhi.gob.bo/index.php/inicio',
                            'Uruguay ' : 'https://www.inumet.gub.uy/clima/estadisticas-climatologicas/tablas-estadisticas',
                            'Colombia' : 'http://meteocolombia.com.co/mtc/',
                            'Paraguay ' : 'https://www.meteorologia.gov.py/',
                            'Argentina ' : 'https://www.smn.gob.ar/                            ',
                        },
                    },
                },       
                
                3: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "UTILES-DATA",
                    enlaces: {
                        estilos: "bg-primary text-white",
                        sitios: {
                            'Ogimet': 'http://www.ogimet.com/cgi-bin/gclimat?mode=1&state=Chil&ind=&ord=REV&verb=no&year=2019&mes=09&months=',
                            'Ecuaciones': 'https://www.tutiempo.net/meteorologia/ecuaciones.html',
                            'Climate Hazards Center': 'https://data.chc.ucsb.edu/',
                        },
                    },
                },       
                
            },
        },
        2: {
            titulo: "🐧 Proyectos WMO ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 WIS2.0 Produccion",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            "MinIO": "http://10.2.62.136:9001/",
                            "Grafana": "http://10.2.62.136:3000",
                            "Wis2Box": "https://wischile.meteochile.gob.cl",
                            "Wis2Box API": "https://wischile.meteochile.gob.cl/oapi",
                        },
                    },
                },

                1: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 WIS2.0 QA",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            "MinIO": "http://qwis2box.dgac.gob.cl:9001/",
                            "Grafana": "http://qwis2box.dgac.gob.cl:3000",
                            "Wis2Box": "http://qwis2box.dgac.gob.cl",
                            "Wis2Box API": "https//qwis2box.dgac.gob.cl/oapi",
                        },
                    },
                },

                2: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 WIS2.0 APUNTES",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            "WIS2.0_Apuntes": "https://docs.google.com/document/d/1lHJzD1Itto85ushSc_X7p2PS4UQcwe2-vw7Svm-yBD4/edit#heading=h.a3idxqk9ivvg",
                            "WIS2.0_Para_TIC": "https://docs.google.com/document/d/1H0Vg9d1OzrHzzA5vKrmIgcMH6kW7anYim6VR777YI_M/edit#heading=h.lxhbwchcr4hl",
                            "WIS2_Entrenamiento": "https://training.wis2box.wis.wmo.int/",
                            "WIS2.0_Instalacion": "https://docs.google.com/document/d/1VZIxnrwUUtMj8FM0VLbkmkz5RkMQIFknSnmPKdPfR50/edit#heading=h.lxhbwchcr4hl",
                        },
                    },
                },

                3: {
                    estilos: "bg-fondo02 text-white",
                    titulo: "🍁 Varios",
                    enlaces: {
                        estilos: "bg-success text-white",
                        sitios: {
                            'WDQMS' : 'https://wdqms.wmo.int',
                            'WIS2Box' : 'https://docs.wis2box.wis.wmo.int/en/latest/',
                            'Oscar/API' : 'https://oscar.wmo.int/surface/rest/api/stations/station/11981/stationReport',
                            'Video/OSCAR' : 'https://vimeo.com/380690445                            ',
                            'Oscar/Surface' : 'https://oscar.wmo.int/surface/',
                            'Puntos Focales' : 'https://community.wmo.int/focal-points-0',
                            'Gisc - Alemania' : 'https://gisc.dwd.de/wisportal/#SearchPlace:Search',
                            'Community Platform' : 'https://community.wmo.int/activity-areas/community-platform',
                        },
                    },
                },
            },
        },
        3: {
            titulo: "🐧 Estaciones Automáticas",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 EMAS",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'NOAA DATA' : 'https://dcs2.noaa.gov/Messages/List?Grid-sort=TblDcpDataDtMsgEnd-desc&Grid-page=1&Grid-pageSize=20&Grid-group=&Grid-filter=',
                            'NOAA SITE' : 'https://www.ncei.noaa.gov/products/land-based-station',
                            'Isla Mocha' : 'https://climatologia.meteochile.gob.cl/application/diario/visorDeDatosEma/380082',
                            'Ceazamet-Json' : 'http://www.ceazamet.cl/ws/pop_ws.php?fn=GetListaEstaciones&p_cod=rlema&tipo_resp=json',
                            'Monitoreo Informática' : 'http://192.168.59.7/intranet/MonitoreoEmas/monitoreo.php',
                        },
                    },
                },
                1: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 PROYECTO ENANDES",
                    enlaces: {
                        estilos: "bg-secondary text-white",
                        sitios: {
                            'Work Plan' : 'https://docs.google.com/spreadsheets/d/1NujoWD9G1A21SKJalbHIfsJVICT8sinK/edit#gid=349624209',
                            'Carpeta Joel' : 'https://drive.google.com/drive/folders/1TdlNYryJj9SKW-Cm_j-iZbJ50eXAKUg3',
                            'Programa CAJA' : 'https://docs.google.com/spreadsheets/d/11EJG15Hnk6yON0F2G0U07qEvO_viFObz/edit#gid=758404417',
                            'Subir Documentos' : 'https://drive.google.com/drive/folders/1IisOCyghBYOhcMpnxS5HVIGjxbD1tjmu',
                        },
                    },
                },

            },
        },
        4: {
            titulo: "🐧 Clave BUFR ",
            estilos: "bg-success text-white",
            temas: {
                0: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " 🐧 Información ",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Ogimet ' : 'https://ogimet.com/bufr.phtml',
                            'GitHub - Libecbufr' : 'https://github.com/ECCC-MSC/libecbufr',
                            'TABLE_D' : 'https://vocabulary-manager.eumetsat.int/vocabularies/BUFR/WMO/26/TABLE_D/307091',
                            'WMOCodes' : 'https://www.wmo.int/pages/prog/www/WMOCodes.html',
                            'BUFR+templates' : 'https://confluence.ecmwf.int/display/ECC/BUFR+templates',
                            'Guide-binary-1A' : 'https://www.wmo.int/pages/prog/www/WDM/Guides/Guide-binary-1A.html',
                        },
                    },
                },

                1: {
                    estilos: "bg-fondo01 text-dark",
                    titulo: " Codificadores ",
                    enlaces: {
                        estilos: "bg-white text-dark",
                        sitios: {
                            'Chile ' : 'https://climatologia.meteochile.gob.cl/application/diario/codificadorBufr',
                            'kunden.dwd.de' : 'https://kunden.dwd.de/bufrviewer/uploadFile',
                            'gisc.kma.go.kr ' : 'http://gisc.kma.go.kr/openwis-user-portal/srv/en/tdcf.home',
                            'aws-bufr-webapp' : 'http://aws-bufr-webapp.s3-website-ap-southeast-2.amazonaws.com/',
                        },
                    },
                },
            },
        },
    },
};
