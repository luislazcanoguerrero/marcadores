export const programacion = {
    titulo: "Programacion",
    order: [3],
    columnas: {
        3: {
            titulo: "🦉 Lenguajes",
            estilos: "bg-danger text-white",
            temas: {
                1: {
                    estilos: "bg-warning text-dark",
                    titulo: "➋ Apuntes",
                    enlaces: {
                        estilos: "bg-dark text-white",
                        sitios: {
                            'Redes' : 'https://docs.google.com/document/d/1W-APqmi8lOzTYpNWUs6iCrvRXGDqSvqeq0y6rNbjxCI/edit',
                            'Docker' : 'https://docs.google.com/document/d/1Fsgg_TMPSquo17PZF6slzVCNx_JM2NAJOuA8gAjmaY8/edit#heading=h.7ea1agcov9a',
                            'NodeJS' : 'https://docs.google.com/document/d/1nGAlZHlCTCpTclatu6w4-afkTyJCRwkJRGu5iF7NY74/edit',
                            'Sonar-QT' : 'https://docs.google.com/document/d/1AdjXQ1TXazFGSkBMd-Q48FXami7yj68mb3etxlUKowo/edit',
                            'Mantis BT' : 'https://docs.google.com/document/d/1tCI6wZxe-jRoIp5FdjFZKkYahCT0_OIwtLXMFEKoODs/edit',
                            'Pentesting' : 'https://docs.google.com/document/d/1PSM9uMsJYS-vyTxiSf6aISMgveEljbXL5r71xQqm7vk/edit#heading=h.7ea1agcov9a',
                            'Java Script' : 'https://docs.google.com/document/d/1GNH6G5Z5hdR7nLdrT0dcg5oA-T1LcCpiBAp4r-zu2lY/edit',
                            'Inyección Laminas' : 'https://docs.google.com/document/d/1LY5hK_bIDsotVSy_jgF5cyrtHz9MZosPjQpeEWpwrzo/edit',
                            'Administración Linux' : 'https://docs.google.com/document/d/1qJBSx5Gjc-VcXLwye_Wl3RuizmNUUxxjkyqvzGadhEE/edit'
                        },
                    },
                },
            },
        },
    },
};
