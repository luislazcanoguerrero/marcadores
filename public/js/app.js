import {marcadores} from './marcadores.js'

const botonera = document.querySelector("#pills-tab")
const contenedores = document.querySelector("#pills-tabContent")

//1. Agregando el botones principales
Object.entries(marcadores).forEach(([keyTabs, tabs]) => {
    let titulo = tabs.titulo
    let li = document.createElement('li')
    li.classList = 'nav-item'
    let boton = document.createElement('button')
    let active = (keyTabs == 0) ? 'active' : ''
    boton.classList = `nav-link ${active}`
    boton.setAttribute('id',`pill-${titulo}-tab`)
    boton.setAttribute('data-bs-toggle',"pill")
    boton.setAttribute('data-bs-target',`#pills-${titulo}`)
    boton.setAttribute('type','button')
    boton.setAttribute('rol','tab')
    boton.setAttribute('aria-controls',`pill-${titulo}`)
    boton.setAttribute('aria-selected','true')
    let span = document.createElement('span')
    span.classList='text-white'
    span.innerHTML=`${titulo}`
    boton.appendChild(span)
    li.appendChild(boton)
    botonera.appendChild(li)
});


//2. Agregando los contenedores
Object.entries(marcadores).forEach(([keyTabs, tabs]) => {
    let titulo = tabs.titulo
    //1. Agregando el boton

    let contenedor = document.createElement("div")
    
    let show = (keyTabs == 0) ? 'show active' : ''
    let numero;
     
    contenedor.classList = `tab-pane ${show} text-white`
    contenedor.setAttribute('id', `pills-${titulo}`)
    contenedor.setAttribute('role', 'tabpanel')
    contenedor.setAttribute('aria-labelledby', `pills-${titulo}-tab`)
    
    let divrow = document.createElement('div')
    divrow.classList = 'row mt-0 ml-2 mb-5'
    tabs.order.forEach((e) => {
        let columna = tabs.columnas[e]
        let divcol = document.createElement('div')
        divcol.classList = 'col-md'

        let divcard = document.createElement('div')
        divcard.classList = `card mt-1 bg-dark` 
        
        let cardHeader = document.createElement('div')
        cardHeader.classList = `card-header ${columna.estilos}`
        cardHeader.innerHTML = `${columna.titulo.toUpperCase() }`
        divcard.appendChild(cardHeader)
        let cardBody = document.createElement('div')
        cardBody.classList = 'card-body p-2 mt-1'
        
        Object.entries(columna.temas).forEach(([keyTemas, temas]) => {
            let {titulo, estilos, enlaces} = temas
            let tema = document.createElement('div')
            tema.classList = 'list-group'
            let pe = document.createElement('p')
            let margenSuperior = (keyTemas == 0) ? 'mt-1' : 'mt-3'
            pe.classList = `list-group-item ${estilos} pt-1 pb-0 mb-0 ${margenSuperior}`
            pe.innerHTML = titulo.toUpperCase()
            tema.appendChild(pe)
            cardBody.appendChild(tema)
            numero = 1;
            Object.entries(enlaces.sitios).forEach(([keySitio, sitio]) => {
                if (sitio === 'x'){
                    let enlace = document.createElement('h6')
                    enlace.classList = `mb-0 mt-2 ${enlaces.estilos}`
                    enlace.innerHTML = `<small>${keySitio} </small>`  
                    tema.appendChild(enlace)
                }
                else if (sitio === 'y') {
                    let enlace = document.createElement('hr')
                    enlace.classList = 'm-1 p-0'
                    tema.appendChild(enlace)
                }else{ 
                    let enlace = document.createElement('a')
                    enlace.classList = `list-group-item pl-1 pt-0 pb-0 ${enlaces.estilos}`
                    enlace.setAttribute('target', '_blank')
                    enlace.setAttribute('href', `${sitio}`)
                    enlace.innerHTML = `<small>【${numero.toString().padStart(2, '0')}】${keySitio} </small>`  
                    numero = numero + 1;
                    tema.appendChild(enlace)
                }
            })
            divcard.appendChild(cardBody)
            divcol.appendChild(divcard)
            divrow.appendChild(divcol)
            contenedor.appendChild(divrow)
        })
    })
    contenedores.appendChild(contenedor)
});


